FROM python:3.8


# set work directory
WORKDIR /usr/src

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /usr/src/requirements.txt
# install dependencies
# RUN set -eux \
#     && apk add --no-cache --virtual .build-deps build-base \
#         libressl-dev \
#         libffi-dev \
#         gcc \
#         musl-dev \
#         python3-dev \
#         rust \
#         cargo \
#         postgresql-dev \
#         libpq \
#     && pip install --upgrade pip setuptools wheel \
#     && pip install -r /usr/src/app/requirements.txt

RUN pip install --upgrade pip wheel setuptools 

RUN pip install -r /usr/src/requirements.txt

# # Project initialization:
# RUN poetry config virtualenvs.create false \
#     && poetry install --no-dev --no-interaction

COPY ./app /usr/src/app

# CMD gunicorn --bind 0.0.0.0:8000 app.main:app -w 1 -k uvicorn.workers.UvicornWorker --reload --access-logfile - --error-logfile - --log-level info

CMD uvicorn app.main:app --reload --workers 1 --host 0.0.0.0 --port 8000