from decouple import config
from tortoise.models import Q
from app.auth.auth_handler import makepassword, checkpassword
from app.s3 import upload_to_aws
from app.admin import models
from app.business import models as business_models
from app.business import controller as business_controller
from app.consumers import models as consumers_models


async def create_admin(creds):
    hashed_password = makepassword(creds.password)
    _admin = await models.Admin.create(
        username = creds.username,
        password = hashed_password,
        email = creds.email
    )
    return _admin

async def get_admin_by_username(username):
    return await models.Admin.get_or_none(username=username)

async def list_admins():
    return await models.Admin.all().order_by('-id')

# DASHBOARD
async def get_dashboard_data():
    data = {}
    total_customers = await consumers_models.ConsumerUser.all().count()
    total_sellers = await business_models.Business.all().count()
    total_orders = await business_models.Order.all().count()
    orders = await business_models.Order.all().order_by('id')
    orders_counts_by_date = {}
    for order in orders:
        _date = order.created_at.date() 
        if _date not in orders_counts_by_date:
            orders_counts_by_date[_date] = 1
        else:
            orders_counts_by_date[_date] += 1
    orders_chart_data = []
    for key, value in orders_counts_by_date.items():
        temp = {
            'day': key.strftime('%d %b'),
            'orders': value
        }
        orders_chart_data.append(temp)
    data = {
        'total_customers': total_customers,
        'total_sellers': total_sellers,
        'total_orders': total_orders,
        'orders_chart_data': orders_chart_data,
    }
    return data

# SELLERS
async def list_sellers():
    return await business_models.Business.all().order_by('-id')

async def verify_seller(data):
    _seller = await business_models.Business.get_or_none(id=data.business_id)
    if not _seller:
        return
    if data.status == "rejected":
        await _seller.delete()
    else:
        _seller.is_verified = True
        _seller.verification_status = "verified"
        await _seller.save()
    return _seller

async def seller_details(business_id):
    _details = await business_models.Business.get_or_none(id=business_id)
    if _details is None:
        return None
    acc = await business_models.BusinessAccount.get_or_none(business_id = business_id)
    if acc is None:
        acc = await business_controller.create_business_account(_details)
    latest_settlement = await business_models.Settlement.filter(seller = _details).order_by('-id').first() 
    _details = _details.__dict__
    _details['account'] = acc
    _details['latest_settlement'] = latest_settlement
    return _details

async def seller_account(business):
    return await business_models.BusinessAccount.get_or_none(business_id = business['id'])

async def update_seller(data):
    business = await business_models.Business.get_or_none(id=data.business_id)
    business.store_name = data.store_name
    business.first_name = data.first_name
    business.last_name = data.last_name
    business.phone = data.phone
    business.email = data.email
    business.address = data.address
    if data.store_image_base64:
        store_image_key = await upload_to_aws(data.store_image_base64, data.store_image_mime, f'business/{config("SERVER")}{business.id}')
        business.store_image = store_image_key
    await business.save()
    return business

async def get_seller_settlements(business):
    settlements = await business_models.Settlement.filter(seller_id = business['id']).order_by('-id')
    return settlements

async def create_settlement(data):
    business = await business_models.Business.get_or_none(id = data.business_id)
    settlement = await business_models.Settlement.create(
        seller = business,
        amount = data.amount
    )
    acc = await business_models.BusinessAccount.get_or_none(business=business)
    acc.balance = acc.balance - data.amount
    await acc.save()
    return settlement

async def seller_update_menu_item(data):
    menu_item = await business_models.MenuItem.get_or_none(id=data.id)
    menu_item.name = data.name
    menu_item.price = data.price
    menu_item.section = data.section
    menu_item.is_veg = data.is_veg
    if data.image_base64:
        item_image_key = await upload_to_aws(data.image_base64, data.image_mime, f'business/{config("SERVER")}{menu_item.business_id}/menu')
        menu_item.image = item_image_key
    await menu_item.save()
    return menu_item

async def seller_delete_item(item_id: int):
    item = await business_models.MenuItem.get_or_none(id=item_id).delete()
    return item

async def seller_menu(business_id):
    business = await business_models.Business.get_or_none(id=business_id)
    menu = await business_models.MenuItem.all().filter(business = business, is_todays_spl = False).order_by('name')
    return business, menu

async def seller_add_menu_item(data):
    business = await business_models.Business.get_or_none(id = data.business_id)
    if data.image_base64:
        item_image_key = await upload_to_aws(data.image_base64, data.image_mime, f'business/{config("SERVER")}{data.business_id}/menu')
    else:
        item_image_key = None
    menu_item = await business_models.MenuItem.create(
        business = business,
        section = data.section,
        name = data.name,
        price = data.price,
        image = item_image_key,
        is_veg = data.is_veg
    )
    return menu_item

async def seller_add_spl_item(data):
    business = await business_models.Business.get_or_none(id = data.business_id)
    if data.image_base64:
        item_image_key = await upload_to_aws(data.image_base64, data.image_mime, f'business/{config("SERVER")}{data.business_id}/specials')
    else:
        item_image_key = None
    spl_item = await business_models.MenuItem.create(
        business = business,
        section = data.section,
        name = data.name,
        price = data.price,
        image = item_image_key,
        is_veg = data.is_veg,
        is_todays_spl = True
    )
    return spl_item

async def remove_item_campaign(item_id):
    item = await business_models.MenuItem.get(id=item_id)
    item.discount = 0
    await item.save()
    return item

async def list_campaigns(business_id):
    business = await business_models.Business.get_or_none(id = business_id)
    campaign = await business_models.Campaign.filter(business = business)
    return campaign

async def create_campaign(data):
    business = await business_models.Business.get_or_none(id=data.business_id)
    campaign = await business_models.Campaign.create(
        business = business,
        name = data.name,
        discount = data.discount,
    )
    if data.image_base64:
        image_key = await upload_to_aws(data.image_base64, data.image_mime, f'business/campaign/{config("SERVER")}{business.id}')
        campaign.image = image_key
    await campaign.save()
    await business_models.MenuItem.filter(business=business).update(discount = data.discount)
    return campaign

async def end_campaign(campaign_id):
    campaign = await business_models.Campaign.get(id=campaign_id)
    business = await business_models.Business.get(id=campaign.business_id)
    await business_models.MenuItem.filter(business = business).update(discount = 0)
    await campaign.delete()
    return campaign

async def list_seller_todays_spl(business_id):
    ts = await business_models.MenuItem.filter(business_id = business_id, is_todays_spl = True).all().order_by('name')
    return ts

# CATEGORIES
async def list_menu_sections():
    return await business_models.MenuSection.all().order_by('section_name')

async def get_menu_section(menu_section_id):
    return await business_models.MenuSection.get_or_none(id = menu_section_id)

async def update_menu_section(data):
    menu_section = await business_models.MenuSection.get(id=data.id)
    if data.section_name:
        menu_section.section_name = data.section_name
    if data.is_visible or not data.is_visible:
        menu_section.is_visible = data.is_visible
    if data.image_base64:
        image_key = await upload_to_aws(data.image_base64, data.image_mime, f'admin/categories/{menu_section.id}')
        menu_section.image = image_key
    await menu_section.save() 
    return menu_section

async def add_menu_section(data):
    menu_section = await business_models.MenuSection.create(
        section_name = data.section_name,
        is_visible = data.is_visible
    )
    if data.image_base64:
        image_key = await upload_to_aws(data.image_base64, data.image_mime, f'admin/categories/{menu_section.id}')
        menu_section.image = image_key
    await menu_section.save()
    return menu_section

async def delete_menu_section(menu_section_id):
    menu_section = await get_menu_section(menu_section_id)
    await menu_section.delete()
    return menu_section

#ORDERS
async def scan_depth(orders):
    data = []
    for order in orders:
        seller = await business_models.Business.get_or_none(id = order.business_id)
        consumer = await consumers_models.ConsumerUser.get_or_none(id = order.consumer_id)
        order_items = await business_models.OrderItem.filter(order=order).order_by('-id')
        temp = []
        for item in order_items:
            if order.order_type.lower() == 'food':
                _item = await business_models.MenuItem.get_or_none(id=item.item_id)
            elif order.order_type.lower() == 'meat':
                _item = await business_models.RawProduct.get_or_none(id=item.item_id)
            _item = _item.__dict__
            _item['quantity'] = item.quantity
            temp.append(_item)
        order = order.__dict__
        order['items'] = temp
        temp = {
            'order' : order,
            'seller' : seller,
            'consumer' : consumer,
        }
        data.append(temp)
    return data

async def list_orders(limit, offset):
    count =  await business_models.Order.all().count()
    orders = await business_models.Order.all().order_by('-id').limit(limit).offset(offset)
    data = await scan_depth(orders)
    return data, count

async def update_delivery_status(data):
    order = await business_models.Order.get_or_none(id = data.order_id)
    order.is_delivered = data.is_delivered
    await order.save()
    return order

async def cancel_order(data):
    order = await business_models.Order.get_or_none(id = data.order_id)
    if not order:
        return order
    order.is_cancelled = data.is_cancelled
    await order.save()
    return order


async def filter_orders(query: str, limit: int, offset: int):
    count = await business_models.Order.filter(Q(Q(order_id__icontains = query), Q(business__store_name__icontains = query), join_type='OR')).limit(limit).offset(offset).count()
    orders = await business_models.Order.filter(Q(Q(order_id__icontains = query), Q(business__store_name__icontains = query), join_type='OR')).limit(limit).offset(offset).all()
    results = await scan_depth(orders)
    return results, count

# RAW PRODUCTS
async def create_raw_product(data):
    rp = await business_models.RawProduct.create(
        name = data.name,
        price = data.price
    )
    if data.image_base64:
        image_key = await upload_to_aws(data.image_base64, data.image_mime, f'raw_products/{rp.id}')
        rp.image = image_key
    await rp.save()
    return rp

async def update_raw_product(data):
    rp = await business_models.RawProduct.get_or_none(id=data.id)
    rp.name = data.name
    rp.price = data.price
    rp.in_stock = data.in_stock
    if data.image_base64:
        image_key = await upload_to_aws(data.image_base64, data.image_mime, f'raw_products/{rp.id}')
        rp.image = image_key
    await rp.save()
    return rp

async def delete_raw_product(raw_product_id):
    await business_models.RawProduct.get(id=raw_product_id).delete()
    return

async def list_raw_products():
    rp = await business_models.RawProduct.all().order_by('name')
    return rp


#
# USERS
# 

async def list_users(limit, offset):
    users = await consumers_models.ConsumerUser.all().order_by('-id').limit(limit).offset(offset)
    count = await consumers_models.ConsumerUser.all().count()
    return users, count

async def filter_users(query: str, limit: int, offset: int):
    if query == 'fetch_all':
        users, count = await list_users(limit, offset)
        return users, count
    query = consumers_models.ConsumerUser.filter(Q(Q(first_name__icontains = query), Q(last_name__icontains = query), join_type='OR')).limit(limit).offset(offset)
    # count = await consumers_models.ConsumerUser.filter(Q(Q(first_name__icontains = query), Q(last_name__icontains = query), join_type='OR')).limit(limit).offset(offset).count()
    # orders = await consumers_models.ConsumerUser.filter(Q(Q(first_name__icontains = query), Q(last_name__icontains = query), join_type='OR')).limit(limit).offset(offset).all()
    users, count = await query.all(), await query.count() 
    return users, count
