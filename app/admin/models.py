from tortoise.contrib.pydantic import pydantic_model_creator
from tortoise.models import Model
from tortoise import fields

class Admin(Model):
    id                      = fields.IntField(pk=True)
    username                = fields.CharField(max_length=200, null=True, unique=True)
    password                = fields.CharField(max_length=500, null=True)
    email                   = fields.CharField(max_length=200, null=True)
    created_at              = fields.DatetimeField(auto_now_add=True)
    updated_at              = fields.DatetimeField(auto_now=True)
    
    class Meta:
        table='admin'

    def __str__(self):
        return str(self.id)

AdminSerializer = pydantic_model_creator(Admin, name="admin_serializer")