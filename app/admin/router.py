from app.business.models import Campaign
from fastapi import APIRouter, Depends
from app.utils import Response
from app.admin import schemas, controller
from app.auth.auth_handler import checkpassword, signJWTadmin
from app.auth.auth_bearer import JWTBearer

admin = APIRouter()

@admin.get('', dependencies=[Depends(JWTBearer())])
async def list_admins():
    admins = await controller.list_admins()
    return Response(admins, 200)

@admin.post('/create')#, dependencies=[Depends(JWTBearer())])
async def create_admin(credentials: schemas.CreateAdmin):
    _admin = await controller.create_admin(credentials)
    return Response(_admin, 200)

@admin.post('/login')
async def admin_login(credentials: schemas.Credentials):
    _admin = await controller.get_admin_by_username(credentials.username)
    if not _admin:
        return Response({}, 401, False, 'No account found')
    if checkpassword(credentials.password, _admin.password):
        access_token = await signJWTadmin(_admin)
        data = {
            "username" : _admin.username,
            "access_token" : access_token
        }
        return Response(data, 200)
    else:
        return Response({}, 401, False, "Wrong credentials")

# DASHBOARD
@admin.get('/dashboard', dependencies=[Depends(JWTBearer())])
async def dashboard_data():
    data = await controller.get_dashboard_data()
    return Response(data, 200)

# SELLERS
@admin.get('/sellers', dependencies=[Depends(JWTBearer())]) 
async def list_sellers():
    sellers = await controller.list_sellers()
    return Response(sellers, 200)

@admin.post('/sellers/verify', dependencies=[Depends(JWTBearer())]) 
async def verify_sellers(data: schemas.VerifySellers):
    _verify = await controller.verify_seller(data)
    return Response(_verify, 200)


@admin.patch('/sellers/details', dependencies=[Depends(JWTBearer())])
async def seller_details_update(data: schemas.SellerUpdate):
    update_seller = await controller.update_seller(data)
    return Response(update_seller, 200)


@admin.post('/sellers/details/create-settlement') #, dependencies=[Depends(JWTBearer())])
async def create_settlement(data: schemas.CreateSettlement):
    business = await controller.seller_details(data.business_id)
    if not business:
        return Response({}, 404, False, 'Seller not found')
    settlement = await controller.create_settlement(data)
    settlements = await controller.get_seller_settlements(business)
    account = await controller.seller_account(business)
    res = {
        'settlements': settlements,
        'account': account
    }
    return Response(res, 200)


@admin.get('/sellers/details/{business_id}/settlements', dependencies=[Depends(JWTBearer())])
async def seller_settlements(business_id: int):
    business = await controller.seller_details(business_id)
    if not business:
        return Response({}, 404, False, 'Seller not found')
    settlements = await controller.get_seller_settlements(business)
    acc = await controller.seller_account(business)
    data = {
        'seller': business,
        'account': acc,
        'settlements': settlements,  
    }
    return Response(data, 200)


@admin.get('/sellers/details/{business_id}', dependencies=[Depends(JWTBearer())])
async def seller_details(business_id: int):
    _details = await controller.seller_details(business_id)
    if not _details:
        return Response({}, 404, False, 'Seller not found')
    return Response(_details, 200)


@admin.post('/sellers/menu/update', dependencies=[Depends(JWTBearer())])
async def seller_update_menu_item(data: schemas.UpdateMenuItem):
    item = await controller.seller_update_menu_item(data)
    return Response(item, 200)


@admin.post('/sellers/menu/add', dependencies=[Depends(JWTBearer())])
async def seller_add_menu_item(data: schemas.AddMenuItem):
    item = await controller.seller_add_menu_item(data)
    return Response(item, 200)

@admin.delete('/sellers/menu/delete/{item_id}', dependencies=[Depends(JWTBearer())])
async def seller_delete_item(item_id: int):
    item = await controller.seller_delete_item(item_id)
    return Response(item, 200)

@admin.get('/sellers/menu/{business_id}', dependencies=[Depends(JWTBearer())])
async def seller_menu(business_id: int):
    seller, menu = await controller.seller_menu(business_id)
    campaigns = await controller.list_campaigns(business_id)
    data = {
        'seller' : seller,
        'menu' : menu,
        'campaign' : campaigns
    }
    return Response(data, 200)

@admin.delete('/sellers/menu/item/{item_id}/campaign', dependencies=[Depends(JWTBearer())])
async def remove_item_campaign(item_id: int):
    item = await controller.remove_item_campaign(item_id)
    return Response(item, 200)

@admin.post('/sellers/campaign', dependencies=[Depends(JWTBearer())])
async def create_campaign(data: schemas.CreateCampaign):
    campaign = await controller.create_campaign(data)
    return Response(campaign, 200)

@admin.get('/sellers/campaign/{business_id}', dependencies=[Depends(JWTBearer())])
async def get_campaigns(business_id: int):
    campaigns = await controller.list_campaigns(business_id)
    return Response(campaigns, 200)

@admin.delete('/sellers/campaign/{business_id}', dependencies=[Depends(JWTBearer())])
async def end_campaign(business_id: int):
    campaign = await controller.end_campaign(business_id)
    return Response(campaign, 200)

@admin.get('/todays-spl/{business_id}')
async def list_seller_todays_spl(business_id: int):
    ts = await controller.list_seller_todays_spl(business_id)
    return Response(ts, 200)

@admin.post('/todays-spl')
async def create_spl_item(data: schemas.AddMenuItem):
    spl_item = await controller.seller_add_spl_item(data)
    return Response(spl_item, 200)


# SECTION
@admin.get('/menu-section', dependencies=[Depends(JWTBearer())])
async def list_menu_sections():
    _sections = await controller.list_menu_sections()
    return Response(_sections, 200)

@admin.patch('/menu-section', dependencies=[Depends(JWTBearer())])
async def update_menu_section(data: schemas.UpdateMenuSection):
    _menu_section = await controller.get_menu_section(data.id)
    if not _menu_section:
        return Response({}, 404, False, "Menu Section not found")
    _update = await controller.update_menu_section(data)
    return Response(_update, 200)

@admin.post('/menu-section', dependencies=[Depends(JWTBearer())])
async def add_menu_section(data: schemas.AddMenuSection):
    menu_section = await controller.add_menu_section(data)
    return Response(menu_section, 200) 

@admin.delete('/menu-section/{menu_section_id}', dependencies=[Depends(JWTBearer())])
async def delete_menu_section(menu_section_id: int):
    menu_section = await controller.get_menu_section(menu_section_id)
    if not menu_section:
        return Response({}, 404, False, "Menu Section not found")
    _delete_section = await controller.delete_menu_section(menu_section_id)
    return Response(_delete_section, 200)

#ORDERS
@admin.get('/orders', dependencies=[Depends(JWTBearer())])
async def list_orders(limit: int = 10, offset: int = 0):
    orders, count = await controller.list_orders(limit, offset)
    data = {
        'orders' : orders,
        'count' : count
    }
    return Response(data, 200)

@admin.get('/orders/search/{query}')
async def filter_orders(query: str, limit: int = 10, offset: int = 0):
    orders, count = await controller.filter_orders(query, limit, offset)
    data = {
        'orders': orders,
        'count': count
    }
    return Response(data, 200)

@admin.post('/orders/update-delivery-status', dependencies=[Depends(JWTBearer())])
async def update_delivery_status(data: schemas.UpdateDeliveryStatus):
    await controller.update_delivery_status(data)
    orders, count = await controller.list_orders(limit=data.limit, offset=data.offset)
    data = {
        'orders': orders,
        'count': count
    }
    return Response(data, 200)

@admin.post('/orders/cancel', dependencies=[Depends(JWTBearer())])
async def cancel_order(data: schemas.CancelOrder):
    print(data)
    await controller.cancel_order(data)
    orders, count = await controller.list_orders(limit=data.limit, offset=data.offset)
    data = {
        'orders': orders,
        'count': count
    }
    return Response(data, 200)

# RAW PRODUCTS
@admin.post('/raw-products', dependencies=[Depends(JWTBearer())])
async def create_raw_product(data: schemas.CreateRawProduct):
    rp = await controller.create_raw_product(data)
    return Response(rp, 200)

@admin.patch('/raw-products', dependencies=[Depends(JWTBearer())])
async def update_raw_product(data: schemas.UpdateRawProduct):
    rp = await controller.update_raw_product(data)
    return Response(rp, 200)

@admin.delete('/raw-products/{raw_product_id}', dependencies=[Depends(JWTBearer())])
async def delete_raw_product(raw_product_id: int):
    await controller.delete_raw_product(raw_product_id)
    return Response({'detail': 'deleted'}, 200)

@admin.get('/raw-products', dependencies=[Depends(JWTBearer())])
async def list_raw_products():
    rp = await controller.list_raw_products()
    return Response(rp, 200)

#
# USERS
#

@admin.get('/users', dependencies=[Depends(JWTBearer())])
async def list_users(limit: int = 20, offset: int = 0):
    users, count = await controller.list_users(limit, offset)
    data = {
        'users': users,
        'count': count
    }
    return Response(data, 200)

@admin.get('/users/search/{query}')
async def filter_users(query: str, limit: int = 10, offset: int = 0):
    users, count = await controller.filter_users(query, limit, offset)
    data = {
        'users': users,
        'count': count
    }
    return Response(data, 200)