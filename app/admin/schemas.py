from app.business.models import Business
from typing import Optional
from pydantic import BaseModel, Field

class Credentials(BaseModel):
    username: str = Field(...)
    password: str = Field(...)
    class Config:
        orm_mode=True

class CreateAdmin(BaseModel):
    username: str = Field(...)
    password: str = Field(...)
    email: str = Field(...)
    class Config:
        orm_mode=True

# SELLERS
class VerifySellers(BaseModel):
    business_id: int = Field(...)
    status: str = Field(...)
    class Config:
        orm_mode = True

class UpdateMenuItem(BaseModel):
    business_id: int
    id: int
    image: str = Field(None)
    image_base64: Optional[str]
    image_mime: Optional[str]
    name: str
    price: int
    is_veg: bool
    section: str = Field(None)
    class Config:
        orm_mode = True

class AddMenuItem(BaseModel):
    business_id: int
    image: str = Field(None)
    image_base64: Optional[str]
    image_mime: Optional[str]
    name: str
    price: int
    is_veg: bool
    section: str = Field(None)
    class Config:
        orm_mode = True

class CreateCampaign(BaseModel):
    business_id: int
    name: str
    discount: int
    image_base64: str = Field(None)
    image_mime: str = Field(None)
    class Config:
        orm_mode = True

class CreateSettlement(BaseModel):
    business_id: int
    amount: int
    class Config:
        orm_mode = True

# SECTION
class AddMenuSection(BaseModel):
    section_name: str = Field(...)
    is_visible: bool =Field(...)
    image_base64: str = Field(None)
    image_mime: str = Field(None)
    class Config:
        orm_mdoe=True

class UpdateMenuSection(BaseModel):
    id: int = Field(...)
    section_name: str = Field(None)
    is_visible: bool = Field(None)
    image_base64: str = Field(None)
    image_mime: str = Field(None)
    class Config:
        orm_mode=True

class SellerUpdate(BaseModel):
    business_id: int = Field(...)
    store_name: Optional[str]
    first_name: Optional[str]
    last_name: Optional[str]
    phone: Optional[str]
    email: Optional[str]
    address: Optional[str]
    store_image_base64: Optional[str]
    store_image_mime: Optional[str]
    class Config:
        orm_mode = True

# ORDERS
class UpdateDeliveryStatus(BaseModel):
    order_id: int = Field(...)
    is_delivered: bool = Field(...)
    limit: int = Field(...)
    offset: int = Field(...)
    class Config:
        orm_mode = True


class CancelOrder(BaseModel):
    order_id: int = Field(...)
    is_cancelled: bool = Field(...)
    limit: int = Field(...)
    offset: int = Field(...)
    class Config:
        orm_mode = True

# RAW PRODUCTS
class CreateRawProduct(BaseModel):
    name: str = Field(...)
    price: str = Field(...)
    image_base64: str = Field(None)
    image_mime: str = Field(None)
    class Config:
        orm_mode = True

class UpdateRawProduct(BaseModel):
    id: int = Field(...)
    name: str = Field(None)
    price: int = Field(None)
    image_base64: str = Field(None)
    image_mime: str = Field(None)
    # discount: int = Field(None)
    in_stock: bool = Field(None)
    class Config:
        orm_mode = True