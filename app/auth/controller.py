from app.business.models import Business
from app.consumers.models import ConsumerUser, Cart
from app.auth.models import OTP, DeviceToken

async def save_otp(data):
    otp = await OTP.create(
        phone = data['phone'],
        otp = data['otp']
    )
    return otp

async def delete_otp(otp_obj):
    return await OTP.get(id=otp_obj.id).delete()

async def get_saved_otp(phone: str, otp: str):
    return await OTP.get(phone=phone, otp=otp)

async def get_user_by_phone(data):
    return await ConsumerUser.get_or_none(phone = data.phone)

async def create_user(data):
    consumer = await ConsumerUser.create(
        phone = data.phone
    )
    cart = await Cart.create(consumer = consumer)
    print('INITIAL CART ', cart)
    return consumer

async def get_business_by_phone(data):
    return await Business.get_or_none(phone = data.phone)

async def update_or_create_device_token(data):
    if data.old_token:
        device_token = await DeviceToken.get_or_none(token=data.old_token)
        if not device_token:
            device_token = await DeviceToken.create(
                token= data.token,
                user_id = data.user_id,
                user_type = data.user_type,
                old_token = data.old_token
            )
            return
        device_token.old_token = data.old_token
        device_token.token = data.token
        await device_token.save()
    else:
        device_token = await DeviceToken.create(
            token= data.token,
            user_id = data.user_id,
            user_type = data.user_type,
            old_token = data.old_token
        )
    return
    

    