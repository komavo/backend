from tortoise.contrib.pydantic import pydantic_model_creator
from tortoise.models import Model
from tortoise import fields

class OTP(Model):
    id = fields.IntField(pk=True)
    phone = fields.CharField(max_length=20)
    otp = fields.CharField(max_length=10)

    def __str__(self):
        return str(self.id)

class DeviceToken(Model):
    id = fields.IntField(pk=True)
    token = fields.CharField(max_length=500)
    user_id = fields.IntField()
    user_type = fields.CharField(max_length=20)
    old_token = fields.CharField(max_length=500, null=True)

    class Meta:
        table='device_token'
    
    def __str__(self):
        return str(self.id)