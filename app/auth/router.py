from fastapi import APIRouter
from app.utils import Response
from app.auth import schemas, utils, controller, auth_handler
from app.auth.auth_bearer import JWTBearer
from app.business import controller as business_controller

from app.auth.auth_handler import signJWT

auth = APIRouter()

@auth.post('/login')
async def login(data: schemas.LoginSchema):
    if data.phone and not data.otp:
        if data.user_type == 'business':
            business = await controller.get_business_by_phone(data)
            if not business:
                return Response({}, 404, False, 'User not found')
            if business and not business.is_verified:
                return Response({}, 404, False, 'User not found')
            if not business.is_verified:
                return Response({}, 200, True, business['verification_status'])
        otp = await utils.sendOTP(data.phone)
        return Response({ 'otp' : otp }, 200, True, 'Otp has been sent')
    else:
        verify_otp = await utils.verifyOTP(data.phone, data.otp)
        if not verify_otp:
            return Response({}, 400, False, 'OTP did not match')
        if data.user_type == 'business':
            business = await controller.get_business_by_phone(data)
            if business and not business.is_verified:
                return Response({}, 404, False, 'User not found')
            if not business:
                return Response({}, 404, False, 'User not found')
            if not business.is_verified:
                return Response({}, 200, True, business['verification_status'])
            token = await auth_handler.signJWT(business)
            account = await business_controller.create_business_account(business)
            business = business.__dict__
            business['access_token'] = token
            return Response(business, 200)
        else:    
            user = await controller.get_user_by_phone(data)
            if not user:
                db_user = await controller.create_user(data)
                token = await auth_handler.signJWT(db_user)
                db_user = db_user.__dict__
                db_user['access_token'] = token
                return Response(db_user, 200)
            else:
                token = await auth_handler.signJWT(user)
                user = user.__dict__
                user['access_token'] = token
                return Response(user, 200)
        

@auth.post('/device-token')
async def update_device_token(data: schemas.UpdateDeviceToken):
    print(f'UPDATE device token for {data.user_type} {data.user_id}')
    await controller.update_or_create_device_token(data)
    return Response(data, 200, True)