from typing import Optional
from pydantic import BaseModel, Field

class SocialLoginSchema(BaseModel):
    token: str = Field(...)
    class Config:
        orm_mode = True

class LoginSchema(BaseModel):
    user_type: str = Field(...)
    phone: str = Field(...)
    otp: str = Field(None)
    class Config:
        orm_mode = True

class UpdateDeviceToken(BaseModel):
    token: str = Field(...)
    old_token: str = Field(None)
    user_id: int = Field(...)
    user_type: str = Field(...)
    class Config:
        orm_mode = True