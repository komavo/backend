import random, requests, os
import urllib.request
import urllib.parse

from twilio.rest import Client
from decouple import config
from jwt.api_jwt import decode
from app.auth import controller, auth_handler, models
from fastapi import HTTPException
 

TEXTLOCAL_API_KEY = config('TEXTLOCAL_API_KEY')


# Find your Account SID and Auth Token at twilio.com/console
def twilio_send_otp(generated_otp, phone):
    account_sid = config('TWILIO_ACCOUNT_SID')
    auth_token = config('TWILIO_AUTH_TOKEN')
    client = Client(account_sid, auth_token)

    try: 
        service = client.messages.create(
            body=f'{generated_otp} is the required Komavo OTP, Bon Appétit !',
            from_=config("FROM_NUMBER"),
            to=f'+91{phone}'
        )
    except:
        service = None
    return

def textlocal_send_otp(otp, phone):
    # message = f'{otp} is the required Komavo OTP. Bon Appétit!'
    message = 'hi there'
    data = urllib.parse.urlencode({'apikey': TEXTLOCAL_API_KEY, 'numbers': '91' + phone, 'message' : message, 'sender': 'komavo'})
    data = data.encode('utf-8')
    request = urllib.request.Request("https://api.textlocal.in/send/?")
    f = urllib.request.urlopen(request, data)
    fr = f.read()
    print(fr)
    return
    
async def sendOTP(phone):
    otp = str(random.random()).split(".")[1][:6]
    data = {
        "phone" : phone,
        "otp" : otp
    }
    twilio_send_otp(otp, phone)
    # textlocal_send_otp(otp, phone)
    save_otp = await controller.save_otp(data)
    return otp

async def verifyOTP(phone, otp):
    saved_otp = await controller.get_saved_otp(phone, otp)
    if saved_otp:
        delete_otp = await controller.delete_otp(saved_otp)
        return True
    else:
        return False
