import secrets
import datetime
from decouple import config
from pypika.terms import Parameter, Interval

from app.auth.models import DeviceToken
from app.s3 import upload_to_aws
from app.business import models, services
from app.consumers import controller as consumer_controller

KOMAVO_COMMISSION = config('KOMAVO_COMMISSION')

async def get_business_user_by_id(business_id):
    return await models.Business.get_or_none(id=business_id)
    
async def get_business_by_phone(phone):
    return await models.Business.get_or_none(phone=phone)

async def create_application(data):
    business = await models.Business.create(
        first_name = data.first_name,
        last_name = data.last_name,
        phone = data.phone,
        email = data.email,
        category = "restaurant",
        region="hojai",
        store_name = data.store_name,
        # store_image = store_image_key,
        address = data.address
    )
    store_image_key = await upload_to_aws(data.store_image, data.store_image_MIME, f'business/{business.id}')
    business.store_image = store_image_key
    await business.save()
    return business

async def update_status(data):
    seller = await models.Business.get_or_none(id=data.business_id)
    seller.is_open = data.status
    await seller.save()
    return seller

async def create_business_account(business):
    acc = await models.BusinessAccount.create(
        business = business,
        balance = 0
    )
    return acc

async def update_business_account(business, amount):
    acc = await models.BusinessAccount.get_or_none(business_id = business.id)
    if acc is None:
        acc = await create_business_account(business)
    acc.balance += float(amount - (amount * (int(KOMAVO_COMMISSION) / 100)))
    await acc.save()
    business.total_revenue += float(amount - (amount * (int(KOMAVO_COMMISSION) / 100)))
    await business.save()
    return acc

#MENU

async def list_menu_sections():
    return await models.MenuSection.all().order_by('section_name')

async def list_menu(business: models.Business):
    return await models.MenuItem.filter(business = business, is_todays_spl = False).all()

async def list_stores():
    sellers = await models.Business.all()
    _sellers = []
    for seller in sellers:
        campaign = await models.Campaign.filter(business=seller)
        seller = seller.__dict__
        seller['campaign'] = campaign
        _sellers.append(seller)
    return _sellers

async def create_menu_item(data):
    _business = await get_business_user_by_id(data.business_id)
    _item = await models.MenuItem.create(
        business = _business,
        name = data.name,
        price = data.price
    )
    if data.image_base64:
        item_image_key = await upload_to_aws(data.image_base64, data.image_mime, f'business/{config("SERVER")}{_business.id}/menu')
        _item.image = item_image_key
    if data.section:
        _item.section = data.section
    await _item.save()
    return _item

async def list_menu_for_consumer(business_id: int):
    business = await get_business_user_by_id(business_id)
    campaign = await models.Campaign.filter(business = business)
    data = {}
    menu = await models.MenuItem.filter(business = business, is_todays_spl = False).all()
    for item in menu:
        if not item.section:
            if 'No Section' not in data:
                data['No Section'] = []
            data['No Section'].append(item)
            continue
        if item.section not in data:
            data[item.section] = []
        data[item.section].append(item)
    return data

async def update_menu_item(data):
    item = await models.MenuItem.get_or_none(id=data.item_id)
    if not item:
        return None
    else:
        if data.section:
            item.section = data.section
        if data.image_base64:
            item_image_key = await upload_to_aws(data.image_base64, data.image_mime, f'business/{config("SERVER")}{item.business_id}/menu')
            item.image = item_image_key
        if data.name:
            item.name = data.name
        if data.price:
            item.price = data.price
        if data.in_stock or not data.in_stock:
            item.in_stock = data.in_stock
        await item.save()
        return item

async def delete_menu_item(item_id: int):
    return await models.MenuItem.filter(id=item_id).delete()

async def list_todays_spl():
    open_businesses = await models.Business.filter(is_open = True).all().order_by('-id')
    res = []
    for b in open_businesses:
        todays_spl = await b.menu_item_store.filter(is_todays_spl = True)
        for item in todays_spl:
            res.append(item.__dict__)
    return res

#ORDERS
async def get_device_tokens(business_id):
    tokens = await DeviceToken.filter(user_id=business_id, user_type='business').all()
    _tokens = []
    for token in tokens:
        _tokens.append(token.token)
    return _tokens

async def list_orders(business: models.Business):
    orders = await models.Order.filter(business=business, created_at__gte = Parameter("CURRENT_DATE") - Interval(days=1)).order_by('-created_at').all() 
    data = []
    for order in orders:
        _details = await order_details(order.order_id)
        data.append(_details)
    return data

async def order_details(order_id: str):
    order = await models.Order.get(order_id=order_id)
    if not order:
        return None
    order_items = await models.OrderItem.filter(order=order).all()
    order = order.__dict__
    order['items'] = []
    for item in order_items:
        _item = await models.MenuItem.get(id=item.item_id)
        _item = _item.__dict__
        _item['quantity'] = item.quantity
        order['items'].append(_item)
    return order

async def create_order(data):
    business = await get_business_user_by_id(data.business_id)
    consumer = await consumer_controller.get_consumer_by_id(data.consumer_id)
    order = await models.Order.create(
        business=business,
        consumer=consumer,
        total=data.total,
        message=data.message,
        delivery_charge=data.delivery_charge
    )
    unique_order_id = f'KMV_{order.id}'
    order.order_id = unique_order_id
    await order.save()
    await update_business_account(business, int(data.total))
    for item in data.items:
        if data.order_type.lower() == 'food':
            _item = await models.MenuItem.get(id=item.item_id)
        elif data.order_type.lower() == 'meat':
            _item = await models.RawProduct.get(id=item.item_id)            
        order_item = await models.OrderItem.create(
            order = order,
            item = _item,
            quantity = item.quantity
        )
        await order_item.save()
    #prepare body for notification
    body = {
        'notification': {
            'title': 'Received a new order',
            'body': f'Order #{order.order_id} received take action now!'
        },
        'priority': 'high',
        "data": {
            "title": "Some title",
            "body": "Some text",
            "click_action": "FLUTTER_NOTIFICATION_CLICK",
            "sound": "default",
            "status": "done",
            "screen": "ORDERS",
            "extradata": "",
        }
    }
    tokens = await get_device_tokens(data.business_id)
    if len(tokens) > 1:
        body['registration_ids'] = tokens
    else:
        body['to'] = tokens[0]
    await services.send_new_order_notfication(body)
    return await order_details(order.order_id)

async def update_order(data):
    print('data', data)
    order = await models.Order.get(order_id = data.order_id)
    if data.status:
        order.status = data.status
    if data.is_cancelled:
        order.is_cancelled = data.is_cancelled
    await order.save()
    return order

async def order_history(consumer_id):
    orders = await models.Order.filter(consumer_id = consumer_id).order_by('-id')
    order_history = []
    for order in orders:
        order_items = await models.OrderItem.filter(order_id = order.id)
        seller = await models.Business.get_or_none(id = order.business_id)
        items = []
        for item in order_items:
            if order.order_type.lower() == 'food':
                _item = await models.MenuItem.get_or_none(id = item.item_id)
            elif order.order_type.lower() == 'meat':
                _item = await models.RawProduct.get_or_none(id = item.item_id)
            _item = _item.__dict__
            _item['quantity'] = item.quantity
            items.append(_item)
        order = order.__dict__
        order['items'] = items
        order['seller'] = seller
        items = []
        order_history.append(order)
    return order_history

# SEARCH
async def search_query(query: str):
    results = {}
    _items = await models.MenuItem.filter(name__contains = query).order_by('-id').all()
    items = []
    for item in _items:
        seller = await models.Business.get(id = item.business_id)
        item = item.__dict__
        item['seller'] = seller
        items.append(item)
    sellers = await models.Business.filter(store_name__contains = query).order_by('-id').all()
    results['sellers'] = sellers
    results['items'] = items
    return results