from tortoise.contrib.pydantic import pydantic_model_creator
from tortoise.models import Model
from tortoise import fields

class Business(Model):
    id                      = fields.IntField(pk=True)
    first_name              = fields.CharField(max_length=200, null=True)
    last_name               = fields.CharField(max_length=200, null=True)
    phone                   = fields.BigIntField()
    email                   = fields.CharField(max_length=200, null=True)
    category                = fields.CharField(max_length=100)
    store_name              = fields.CharField(max_length=100)
    store_image             = fields.CharField(max_length=100, null=True)
    address                 = fields.CharField(max_length=1000)
    region                  = fields.CharField(max_length=100)
    is_verified             = fields.BooleanField(default=False)
    is_open                 = fields.BooleanField(default=True)
    verification_status     = fields.CharField(max_length=50, default='pending')
    total_revenue           = fields.FloatField(default=0.0)
    created_at              = fields.DatetimeField(auto_now_add=True)
    updated_at              = fields.DatetimeField(auto_now=True)
    
    class Meta:
        table='business'

    def __str__(self):
        return 'Business ' +str(self.id)

BusinessSerializer = pydantic_model_creator(Business, name="business_serializer")


class BusinessAccount(Model):
    id                      = fields.IntField(pk=True)
    business                = fields.ForeignKeyField('models.Business', related_name='business_account', on_delete='CASCADE')                
    balance                 = fields.FloatField(default=0.00)
    updated_at              = fields.DatetimeField(auto_now_add=True)

    class Meta:
        table = 'business_account'

    def __str__(self):
        return f'BusinessAccount {str(self.id)}'

class MenuSection(Model):
    id                      = fields.IntField(pk=True)
    section_name            = fields.CharField(max_length=200)
    image                   = fields.CharField(max_length=500, null=True)
    is_visible              = fields.BooleanField(default = False)

    class Meta:
        table='menu_section'
    
    def __str__(self):
        return f'MenuSection {str(self.id)}'

MenuSectionSerializer = pydantic_model_creator(MenuSection, name="menu_section_serializer")

class MenuItem(Model):
    id                      = fields.IntField(pk=True)
    business                = fields.ForeignKeyField('models.Business', related_name='menu_item_store')
    section                 = fields.CharField(max_length=200, null=True)
    image                   = fields.CharField(max_length=500, null=True)
    name                    = fields.CharField(max_length=500)
    price                   = fields.IntField()
    discount                = fields.IntField(default=0)
    in_stock                = fields.BooleanField(default=True)
    is_veg                  = fields.BooleanField(default=True)
    is_todays_spl           = fields.BooleanField(default=False)

    class Meta:
        table='menu_item'
    
    def __str__(self):
        return f'MenuItem {str(self.id)}'

# MenuItemSerializer = pydantic_model_creator(MenuItem, name="menu_item_serializer")

class Order(Model):
    id                      = fields.IntField(pk=True)
    order_id                = fields.CharField(max_length=100, null=True)
    order_type              = fields.CharField(max_length=50, default='food')
    business                = fields.ForeignKeyField('models.Business', related_name='order_seller')
    consumer                = fields.ForeignKeyField('models.ConsumerUser', related_name='order_consumer')
    total                   = fields.IntField(null=True)
    message                 = fields.CharField(max_length=1000, null=True)
    status                  = fields.CharField(max_length=50, default='pending')
    is_delivered            = fields.BooleanField(default=False)
    is_cancelled            = fields.BooleanField(default=False)
    delivery_charge         = fields.IntField()
    created_at              = fields.DatetimeField(auto_now_add=True)
    updated_at              = fields.DatetimeField(auto_now=True)

    class Meta:
        table = 'order'
    
    def __str__(self):
        return 'Order ' + str(self.id)

class OrderItem(Model):
    id                      = fields.IntField(pk=True)
    order                   = fields.ForeignKeyField("models.Order",  related_name='orderitem_cart', on_delete='CASCADE')
    item                    = fields.ForeignKeyField('models.MenuItem', related_name='orderitem_item', on_delete='CASCADE', null=True)
    raw_items               = fields.ForeignKeyField('models.RawProduct', related_name='orderitem_raw', on_delete='CASCADE', null=True)
    quantity                = fields.IntField(default=1)
    class Meta:
        table='order_item'
    def __str__(self):
        return 'OrderItem ' + str(self.id)

class Campaign(Model):
    id                      = fields.IntField(pk=True)
    name                    = fields.CharField(max_length=500, null=True)
    image                   = fields.CharField(max_length=500, null=True)
    business                = fields.ForeignKeyField("models.Business", related_name="business_campaign", on_delete='CASCADE')
    discount                = fields.IntField()
    class Meta:
        table='campaign'
    def __str__(self):
        return 'Campagin ' + str(self.id)

class RawProduct(Model):
    id                      = fields.IntField(pk=True)
    product_id              = fields.CharField(max_length=200, null=True)
    name                    = fields.CharField(max_length=200)
    price                   = fields.IntField()
    image                   = fields.CharField(max_length=500, null=True)
    discount                = fields.IntField(default=0)
    in_stock                = fields.BooleanField(default=True)

    class Meta:
        table='raw_product'
    def __str__(self):
        return 'RawProduct ' + str(self.name)

class RawProductFreebie(Model):
    id                      = fields.IntField(pk=True)
    raw_product             = fields.ForeignKeyField("models.RawProduct", related_name="freebies", on_delete='CASCADE')
    name                    = fields.CharField(max_length=200)
    price                   = fields.IntField()
    image                   = fields.CharField(max_length=500, null=True)

    class Meta:
        table='raw_product_freebie'
    def __str__(self):
        return 'RawProductFreebie ' + str(self.name)

class Settlement(Model):
    id                      = fields.IntField(pk=True)
    seller                  = fields.ForeignKeyField('models.Business', related_name='business_settlements', on_delete='CASCADE')
    amount                  = fields.FloatField(null=True)
    created_at              = fields.DatetimeField(auto_now_add=True)