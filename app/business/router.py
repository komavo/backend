from fastapi import APIRouter
from app.utils import Response
from app.business import schemas, controller, services

business = APIRouter()


@business.get('/detail/{business_id}')
async def get_business_user(business_id: int):
    business_user = await controller.get_business_user_by_id(business_id)
    if not business_user:
        return Response({}, 404, False, "User not found")
    return Response(business_user, 200)

@business.post('/apply')
async def apply_business(data: schemas.ApplyBusiness):
    seller = await controller.get_business_by_phone(data.phone)
    if seller:
        return Response({}, 400, False, 'Phone number exists')
    _application = await controller.create_application(data)
    _application = _application.__dict__
    _application['id'] = str(_application['id'])
    # _application['id'] = str(_application['id'])
    return Response(_application, 200)

@business.post('/update-status')
async def update_status(data: schemas.UpdateStatus):
    result = await controller.update_status(data)
    return Response({}, 200)


#MENU
@business.get('/menu-section')
async def list_menu_section():
    _menu_sections = await controller.list_menu_sections()
    data = []
    for item in _menu_sections:
        temp = {
            'value' : item.section_name,
            'label' : item.section_name
        }
        data.append(temp)
    return Response(data, 200)

@business.post('/menu-item')
async def create_menu_item(data: schemas.CreateMenuItem):
    menu_item = await controller.create_menu_item(data)
    # print(data)
    return Response(menu_item, 200)

@business.get('/menu/{business_id}')
async def list_menu(business_id: int):
    business = await controller.get_business_user_by_id(business_id)
    if not business:
        return Response({}, 404, False, "User not found")
    menu = await controller.list_menu(business)
    return Response(menu, 200)

@business.patch('/menu-item')
async def update_menu_item(data: schemas.UpdateMenuItem):
    item = await controller.update_menu_item(data)
    return Response(item, 200)


@business.delete('/menu-item/{item_id}')
async def delte_menu_item(item_id: int):
    item = await controller.delete_menu_item(item_id)
    return Response({}, 200)

#ORDERS
@business.get('/order/list/{business_id}')
async def list_orders(business_id: int):
    business = await controller.get_business_user_by_id(business_id)
    if not business:
        return Response({}, 404, False, 'Business not found')
    orders = await controller.list_orders(business)
    return Response(orders, 200, True)

@business.get('/order/detail/{order_id}')
async def order_detail(order_id: int):
    print(order_id)
    return Response({}, 200, True)

@business.post('/order/update')
async def order_update(data: schemas.UpdateOrder):
    _update = await controller.update_order(data)
    return Response(_update, 200, True)

@business.get('/send-notification')
async def send_notification():
    result = await services.send_test_notification()
    print(result)
    return Response('ok', 200)
