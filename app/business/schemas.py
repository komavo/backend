from typing import Optional
from pydantic import BaseModel, Field

class UpdateBusiness(BaseModel):
    id: int = Field(...)
    first_name: str = Field(None)
    last_name: str = Field(None)
    phone: str = Field(None)
    email: str = Field(None)
    class Config:
        orm_mode=True

class UpdateStatus(BaseModel):
    business_id: int = Field(...)
    status: bool = Field(...)
    class Config:
        orm_mode=True

class CreateCategory(BaseModel):
    category: str = Field(...)
    class Config:
        orm_mode=True

class CreateStore(BaseModel):
    owner: int = Field(...)
    category: int = Field(...)
    store_name: str = Field(...)
    description: str = Field(...)
    opening: str = Field(...)
    closing: str = Field(...)
    latitude: float = Field(...)
    longitude: float = Field(...)

    class Config:
        orm_mode=True

class CreateMenuSection(BaseModel):
    store: int = Field(...)
    section_name: str = Field(...)
    class Config:
        orm_mode=True

class CreateMenuItem(BaseModel):
    business_id: int = Field(...)
    section: str = Field(None)
    image_base64: str = Field(None)
    image_mime: str = Field(None)
    name: str = Field(...)
    price: int = Field(...)
    class Config:
        orm_mode=True

class UpdateMenuItem(BaseModel):
    item_id: int = Field(...)
    section: str = Field(None)
    image_base64: str = Field(None)
    image_mime: str = Field(None)
    name: str = Field(None)
    price: str = Field(None)
    in_stock: bool = Field(None)
    class Config:
        orm_mode=True 

class ApplyBusiness(BaseModel):
    first_name: str = Field(...)
    last_name: str = Field(...)
    phone: str = Field(...)
    store_name: str = Field(...)
    email: str = Field(...)
    address: str = Field(...)
    store_image: str = Field(...)
    store_image_MIME: str = Field(...)
    class Config:
        orm_mode=True


#ORDER
class UpdateOrder(BaseModel):
    order_id: str = Field(...)
    status: str = Field(None)
    is_cancelled: bool = Field(None)
    class Config:
        orm_mode=True
        