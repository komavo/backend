from pyfcm import FCMNotification
from decouple import config 

FCM_API_KEY = config('FCM_API_KEY')

# proxy_dict = {
#     "http"  : "http://127.0.0.1:8000",
#     "https" : "http://127.0.0.1:8000",
# }

# push_service = FCMNotification(api_key=FCM_API_KEY, proxy_dict=proxy_dict)

# async def send_test_notification():
#     registration_id = "cAXTQc6aS3Wmb9UoOraDic:APA91bEEfbE2qSJjLwr6eLJOxKpV_9kNxjU3H5G0Fuj-HiD4m8hLQa32pF74-9nKSfz209MqU3JnoKSSqEDGfQh2XSXKKEobR3JFlzGwXmYA5TT80PmiaiJdKO1oXcfqCGiDh4XWDqwI"
#     message_title = "Uber update"
#     message_body = "Hi john, your customized news for today is ready"
#     print('sending message')
#     try:
#         result = push_service.notify_single_device(registration_id=registration_id, message_title=message_title, message_body=message_body)
#     except:
#         print('error')
#         return
#     return result


import requests
import json

serverToken = config('FCM_API_KEY')
FCM_BASE_URL = config('FCM_BASE_URL')

async def send_test_notification():
    deviceToken = 'dy07ZVe0QA-bplANJwKr6l:APA91bExe_v34pX4dzJVADQmBaKtvXBnKZKP00Rb7z35_LmUy-oSZeCvc6n_vt6M3e4tUguBM0UHI1r-8XxoRf4nxeDCv4tBQa8UOEogFbobj52Nsekhp1CNPxfAJ_d1yuff3wqPuOVM'
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'key=' + serverToken,
    }

    body = {
        'notification': {
            'title': 'Sending push form python script',
            'body': 'New Message'
        },
        'to': deviceToken,
        'priority': 'high',
        # 'data': {
        #     'key' : 'value'
        # },
        "data": {
            "title": "Some title",
            "body": "Some text",
            "click_action": "FLUTTER_NOTIFICATION_CLICK",
            "sound": "default",
            "status": "done",
            "screen": "ORDERS",
            "extradata": "",
        }
    }
    response = requests.post(FCM_BASE_URL, headers = headers, data=json.dumps(body))
    print(response.status_code)

    print(response.json())
    return response.json()
    # return

async def send_new_order_notfication(body):
    print(body)
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'key=' + serverToken,
    }
    # body = {
    #     'notification': {
    #         'title': 'Sending push form python script',
    #         'body': 'New Message'
    #     },
    #     'to': data['deviceTokens'],
    #     'priority': 'high',
    #     # 'data': {
    #     #     'key' : 'value'
    #     # },
    #     "data": {
    #         "title": "Some title",
    #         "body": "Some text",
    #         "click_action": "FLUTTER_NOTIFICATION_CLICK",
    #         "sound": "default",
    #         "status": "done",
    #         "screen": "ORDERS",
    #         "extradata": "",
    #     }
    # }
    response = requests.post(FCM_BASE_URL, headers = headers, data=json.dumps(body))
    print(response.status_code)
    print(response.json())
    return

