import re
from typing import List
 
from app.business.models import MenuItem, Business, MenuSection
from app.consumers.models import ConsumerUser, Favourite, Cart, CartItem

#PROFILE
async def update_consumer(data):
    consumer = await ConsumerUser.get(id=data.consumer_id)
    if consumer:
        if data.first_name:
            consumer.first_name = data.first_name
        if data.last_name:
            consumer.last_name = data.last_name
        if data.latitude:
            consumer.latitude = data.latitude
        if data.longitude:
            consumer.longitude = data.longitude
        if data.location:
            consumer.location = data.location
        await consumer.save()
        return consumer
    else:
        return None

async def get_consumer_by_id(consumer_id: int):
    return await ConsumerUser.get_or_none(id=consumer_id)

#HOME
async def get_sections():
    sections = await MenuSection.all().order_by('section_name')
    data = []
    for section in sections:
        items = await MenuItem.filter(section = section.section_name)
        if items:
            data.append(section)
    return data

async def get_section_items(section):
    items = await MenuItem.filter(section = section).order_by('id')
    data = []
    for item in items:
        business = await Business.get_or_none(id = item.business_id)
        temp = {
            'item' : item,
            'business' : business
        }
        data.append(temp)
    return data


#CART

async def get_cart(consumer: ConsumerUser):
    cart = await Cart.get_or_none(consumer=consumer)
    if not cart:
        return []
    cart_items = await CartItem.filter(cart = cart).order_by('-id')
    _cart = []
    for item in cart_items:
        _item = await MenuItem.get(id=item.item_id)
        _item = _item.__dict__
        _item['quantity'] = item.quantity
        # item = item.__dict__
        # item['item'] = 
        _cart.append(_item)
    return _cart

async def update_cart(data, consumer: ConsumerUser):
    cart = await Cart.get_or_none(consumer=consumer)
    if not cart:
        cart = await get_cart(consumer)
    item = await MenuItem.get_or_none(id=data.item_id)
    try: 
        cart_item = await CartItem.get(cart=cart, item=item)
        # cart_item = await CartItem.get_or_none(cart=cart)
        cart_item.quantity += data.quantity
        await cart_item.save()
    except:    
        cart_item = await CartItem.create(
            cart = cart,
            item = item,
            quantity = data.quantity
        )
    return await get_cart(consumer)

async def remove_from_cart(item_id, consumer: ConsumerUser):
    cart = await Cart.get_or_none(consumer=consumer)
    item = await MenuItem.get(id=item_id)
    await CartItem.filter(cart=cart, item=item).delete()
    return await get_cart(consumer)

async def clear_cart(consumer: ConsumerUser):
    cart = await Cart.get_or_none(consumer=consumer)
    await CartItem.filter(cart=cart).delete()
    return await get_cart(consumer)

#FAVOURTIES
async def get_favourites(consumer: ConsumerUser):
    favourites = await Favourite.filter(consumer=consumer)
    data = {}
    data['restaurants'] = [] 
    data['items'] = [] 
    for item in favourites:
        if item.business_id:
            _business = await Business.get(id=item.business_id)
            _business = _business.__dict__
            _business['favourite_id'] = item.id
            data['restaurants'].append(_business)
        if item.item_id:
            _item = await MenuItem.get(id=item.item_id)
            _item = _item.__dict__
            _item['favourite_id'] = item.id
            data['items'].append(_item)
    return data
 
async def clear_favourites(consumer: ConsumerUser):
    await Favourite.filter(consumer=consumer).delete()
    return await get_favourites(consumer)

async def add_favourite(data):
    consumer = await ConsumerUser.get(id=data.consumer_id)
    business = await Business.get(id=data.business_id) if data.business_id else None
    item = await MenuItem.get(id=data.item_id) if data.item_id else None    
    favourite = await Favourite.create(
        consumer = consumer,
        business = business,
        item = item 
    )
    return await get_favourites(consumer)

async def remove_favourite(favourite_id: int):
    favourite = await Favourite.get(id=favourite_id)
    consumer = await get_consumer_by_id(favourite.consumer_id)
    await favourite.delete()
    return await get_favourites(consumer)
