
from tortoise.models import Model
from tortoise import fields

class ConsumerUser(Model):
    id                      = fields.IntField(pk=True)
    first_name              = fields.CharField(max_length=100, null=True)
    last_name               = fields.CharField(max_length=100, null=True)
    phone                   = fields.CharField(max_length=20, unique=True)
    latitude                = fields.FloatField(null=True)
    longitude               = fields.FloatField(null=True)
    location                = fields.TextField(null=True)
    user_type               = fields.CharField(max_length=100, default='consumer')
    created_at              = fields.DatetimeField(auto_now_add=True)
    updated_at              = fields.DatetimeField(auto_now=True)

    class Meta:
        table='consumer_user'
    
    def __str__(self):
        return 'ConsumerUser ' + str(self.id)

class Favourite(Model):
    id                      = fields.IntField(pk=True)
    consumer                = fields.ForeignKeyField('models.ConsumerUser', related_name='favourite_consumer', on_delete='CASCADE')
    business                = fields.ForeignKeyField('models.Business', related_name='favourite_business', on_delete='CASCADE', null=True)
    item                    = fields.ForeignKeyField('models.MenuItem', related_name='favourite_item', on_delete='CASCADE', null=True)
    class Meta:
        table='favourites'
    def __str__(self):
        return 'Favourite ' + str(self.id)

class Cart(Model):
    id                      = fields.IntField(pk=True)
    consumer                = fields.ForeignKeyField('models.ConsumerUser', related_name='cart_consumer', on_delete='CASCADE')
    class Meta:
        table='cart'
    def __str__(self):
        return 'Cart ' + str(self.id)

class CartItem(Model):
    id                      = fields.IntField(pk=True)
    cart                    = fields.ForeignKeyField("models.Cart",  related_name='cartitem_cart', on_delete='CASCADE')
    item                    = fields.ForeignKeyField('models.MenuItem', related_name='cartitem_item', on_delete='CASCADE')
    quantity                = fields.IntField(default=1)
    class Meta:
        table='cart_item'
    def __str__(self):
        return 'CartItem ' + str(self.id)


