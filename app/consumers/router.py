from typing import List
from fastapi import APIRouter
from fastapi.exceptions import HTTPException

from app.utils import Response
from app.consumers import schemas, controller
from app.business import controller as business_controller

consumers = APIRouter()

#PROFILE
@consumers.patch('/profile')
async def update_consumer(data: schemas.UpdateConsumer):
    consumer = await controller.update_consumer(data)
    return Response(consumer, 200)

@consumers.get("/profile/{consumer_id}")
async def get_consumer_user(consumer_id: int):
    consumer = await controller.get_consumer_by_id(consumer_id)
    if not consumer:
        return Response({}, 404, False, 'Consumer not found')
    return Response(consumer, 200, True )

#HOME
@consumers.get('/sections')
async def get_sections():
    sections = await controller.get_sections()
    return Response(sections, 200)

@consumers.get('/section/{section_name}/items')
async def get_section_items(section_name: str):
    data = await controller.get_section_items(section_name)
    return Response(data, 200)

# OTHERS
@consumers.get('/todays-special')
async def list_todays_special():
    ts = await business_controller.list_todays_spl()
    return Response(ts, 200)


#CART
@consumers.post('/cart')
async def update_cart(data: schemas.UpdateCart):
    consumer = await controller.get_consumer_by_id(data.consumer_id)
    if not consumer:
        return Response({}, 404, False, 'Consumer not found')
    add_to_cart = await controller.update_cart(data, consumer)
    print(add_to_cart)
    return Response(add_to_cart, 200, True)

@consumers.post('/cart/remove-item')
async def cart_remove_item(data: schemas.CartRemoveItem):
    consumer = await controller.get_consumer_by_id(data.consumer_id)
    if not consumer:
        return Response({}, 404, False, 'Consumer not found')
    updated_cart = await controller.remove_from_cart(data.item_id, consumer)
    return Response(updated_cart, 200)

@consumers.delete('/cart/{consumer_id}')
async def clear_cart(consumer_id: int):
    consumer = await controller.get_consumer_by_id(consumer_id)
    if not consumer:
        return Response({}, 404, False, 'Consumer not found')
    cart = await controller.clear_cart(consumer)
    return Response(cart, 200, True)

@consumers.get('/cart/{consumer_id}')
async def get_cart(consumer_id: int):
    consumer = await controller.get_consumer_by_id(consumer_id)
    if not consumer:
        return Response({}, 404, False, 'Consumer not found')
    cart = await controller.get_cart(consumer)
    return Response(cart, 200, True)



#FAVOURTIES
@consumers.post('/favourites')
async def add_favourite(data: schemas.AddFavourite):
    favourites = await controller.add_favourite(data)
    return Response(favourites, 200, True)

@consumers.post('/favoutires/remove/{favourite_id}')
async def remove_favourite(favourite_id: int):
    favourites = await controller.remove_favourite(favourite_id)
    return Response(favourites, 200, True)

@consumers.get('/favourites/{consumer_id}')
async def get_favourites(consumer_id: int):
    consumer = await controller.get_consumer_by_id(consumer_id)
    if not consumer:
        return Response({}, 404, False, 'Consumer not found')
    favourites = await controller.get_favourites(consumer)
    return Response(favourites, 200, True)

@consumers.delete('/favourites/{consumer_id}')
async def clear_favourites(consumer_id: int):
    consumer = await controller.get_consumer_by_id(consumer_id)
    if not consumer:
        return Response({}, 404, False, 'Consumer not found')
    favourites = await controller.clear_favourites(consumer)
    return Response(favourites, 200, True)



#BUSINESS 
@consumers.get("/store")
async def list_stores():
    stores = await business_controller.list_stores()
    return Response(stores, 200, True)

@consumers.get("/store/{business_id}/menu")
async def get_store_menu(business_id: int):
    menu = await business_controller.list_menu_for_consumer(business_id)
    return Response(menu, 200, True)


#ORDERS
@consumers.post('/order')
async def create_order(data: schemas.CreateOrder):
    order = await business_controller.create_order(data)
    return Response(order, 200, True)

@consumers.get('/order-history/{consumer_id}')
async def order_history(consumer_id: int):
    order_history = await business_controller.order_history(consumer_id)
    return Response(order_history, 200, True)


# SEARCH
@consumers.get('/search/{query}')
async def search_query(query: str):
    results = await business_controller.search_query(query)
    return Response(results, 200)

# RAW PRODUCTS
@consumers.get('/raw-products')
async def list_raw_products():
    rps = await business_controller.list_raw_products()
    return Response(rps, 200)
