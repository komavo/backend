from typing import Optional, List
from pydantic import BaseModel, Field

class UpdateConsumer(BaseModel):
    consumer_id: int = Field(...)
    first_name: str = Field(None)
    last_name: str = Field(None)
    latitude: float = Field(None)
    longitude: float = Field(None)
    location: str = Field(None)
    class Config:
        orm_mode = True


#CART
class UpdateCart(BaseModel):
    consumer_id: int = Field(...)
    item_id: int = Field(...)
    quantity: int = Field(...)
    class Config:
        orm_mode = True

class CartRemoveItem(BaseModel):
    consumer_id: int = Field(...)
    item_id: int = Field(...)
    class Config:
        orm_mode = True


#FAVOURITES
class AddFavourite(BaseModel):
    consumer_id: int = Field(...)
    business_id: int = Field(None)
    item_id: int = Field(None)
    class Config:
        orm_mode = True


#ORDERS
class OrderItem(BaseModel):
    item_id: int = Field(...)
    quantity: int = Field(...)
    class Config:
        orm_mode = True

class CreateOrder(BaseModel):
    order_type: str = Field(...)
    consumer_id: int = Field(...)
    business_id: int = Field(...)
    message: str = Field(None)
    delivery_charge: int = Field(...)
    total: int = Field(...)
    items: List[OrderItem]
    class Config:
        orm_mode = True
    