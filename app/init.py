from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from tortoise import Tortoise
from starlette.responses import RedirectResponse, HTMLResponse
from fastapi.middleware.cors import CORSMiddleware
from tortoise.contrib.fastapi import register_tortoise
from app.config import settings
from app.utils import generate_splash

SERVER_PREFIX = '/v2'

def create_app() -> FastAPI:

    app = FastAPI(
        title=settings.APP_NAME, 
        version=settings.APP_VERSION,
        openapi_url = SERVER_PREFIX + '/openapi.json',
        docs_url = SERVER_PREFIX + '/docs',
        redoc_url = SERVER_PREFIX + '/redoc'
    )

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    @app.get('/')
    def main():
        content = generate_splash()
        return HTMLResponse(content=content, status_code=200)

    register_tortoise(
        app,
        db_url=settings.DATABASE_URL,
        modules={"models": ["aerich.models", "app.auth.models", "app.business.models", "app.consumers.models", "app.admin.models"]},
        generate_schemas=True,
        add_exception_handlers=True,
    )
    Tortoise.init_models(['app.auth.models', 'app.business.models', 'app.consumers.models'], 'models') 
    # register_tortoise(app, config=settings.TORTOISE_ORM)
    register_views(app=app)
    return app

def register_views(app: FastAPI):
    from app.auth.router import auth
    from app.business.router import business
    from app.consumers.router import consumers
    from app.admin.router import admin

    app.include_router(admin, prefix=SERVER_PREFIX + '/admin', tags=['Admin'])
    app.include_router(auth, prefix=SERVER_PREFIX + '/auth', tags=["Auth"])
    app.include_router(business, prefix=SERVER_PREFIX + '/business', tags=["Business"])
    app.include_router(consumers, prefix=SERVER_PREFIX + '/consumer', tags=["Consumer"])
    # app.include_router(auth, prefix='/auth', tags=["Auth"])
    # app.include_router(journals, prefix='/templates', tags=["Templates"])
