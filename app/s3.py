import logging
from fastapi import HTTPException, File
import botocore
import boto3
import time
from decouple import config
import base64
from PIL import Image
import io

AWS_ACCESS_KEY_ID = config('S3_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = config('S3_SECRET_ACCESS_KEY')
bucket = config('S3_BUCKET')

CONTENT_TYPES = {
    "image/jpeg" : ".jpg",
    "image/png" : ".png",
    "image/svg+xml" : ".svg",
    "video/mp4" : ".mp4",
    "video/mpeg" : ".m1v",
    "video/webm" : ".mov",
    "video/x-m4v" : ".m4v",
}

async def upload_to_aws(base64Image, imageMIME, storage_folder):
    imageBytes = base64.b64decode(base64Image)
    # image = Image.open(io.BytesIO(imageBytes))

    # if image.content_type == "image/jpeg":
    #     ext = ".jpg"
    # elif image.content_type == "image/png":
    #     ext = ".png"
    try:
        ext = CONTENT_TYPES[imageMIME]
    except:
        raise HTTPException(status_code=403, detail="Invalid file type")

    time_stamp = time.strftime("%Y%m%d_%H:%M:%S")
    storage_path = storage_folder+"/"+time_stamp+"_" + ext
    client = boto3.client('s3', aws_access_key_id=AWS_ACCESS_KEY_ID,aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
    try:
        response = client.put_object(Body=imageBytes, Bucket=bucket, Key=storage_path, ContentType=imageMIME, ACL='public-read')
    except botocore.exceptions.ClientError as error:
        raise error
    return storage_path 