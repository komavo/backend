def Response(data, status_code=200, success=True, message="OK"):
    if success:
        error = False
    else:
        error = True
    return {
        "status_code" : status_code,
        "error" : error,
        "success" : success,
        "message" : message,
        "data" : data,
    }

def generate_splash():
    html_content = """
    <html>
    <head>
        <link rel="icon" href="https://komavo-dev.s3.ap-south-1.amazonaws.com/icon.svg" type="image/gif" sizes="16x16">
        <link rel="icon" href="https://komavo-dev.s3.ap-south-1.amazonaws.com/icon.svg" type="image/gif" sizes="32x32">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@500&display=swap" rel="stylesheet">
        <title>KOMAVO</title>
        <style>
            body{
                font-family: 'Raleway', sans-serif;
            }
            .container{
                margin:0px auto;
                display: flex;
                max-width: 90vw;
                align-items: center;
                justify-content: center;
                flex-direction: column;
            }
            .hero{
                max-width: 90%;
            }
            .logo{
                margin-top: 50px;
                max-width: 350px;
            }
            @media only screen and (min-device-width : 1200px){
                .container { max-width: 50vw }
                .hero { max-width: 400px; }
                .logo { margin-top: 0px; }
            }
            .timer{
                display: flex;
            }
            .days, .hours, .mins, .secs{
                padding: 10px;
                display: flex;
                margin: 10px;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                background-color: #eee;
                box-shadow: 0px 0px 10px #aaa;
                border-radius: 20px;
                color: #aaa;
            }
            #days, #hours, #mins, #secs{
                font-weight: 800;
                background-color: #222;
                color: #aaa;
                font-size: 2rem;
                padding: 20px;
                border-radius: 20px;
            }
        </style>
        <script>
        // Set the date we're counting down to
        var countDownDate = new Date("Sep 10, 2021 12:00:00").getTime();

        // Update the count down every 1 second
        var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        document.getElementById("days").innerHTML = days;
        document.getElementById("hours").innerHTML = hours;
        document.getElementById("mins").innerHTML = minutes;
        document.getElementById("secs").innerHTML = seconds;

        // If the count down is finished, write some text
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo").innerHTML = "EXPIRED";
        }
        }, 1000);
        </script>
    </head>
    <body class='container' style="height:100vh;background-color: #feeccb">
        <img src='https://komavo-dev.s3.ap-south-1.amazonaws.com/home-bg.png' class='hero'/>
        <img src='https://komavo-dev.s3.ap-south-1.amazonaws.com/full-logo.svg' class='logo'/>
        <div style="color: #666 !important; font-size: 1rem; font-weight: 600"> Now in Hojai !!!</div>
        <br/>
        <img src='https://komavo-dev.s3.ap-south-1.amazonaws.com/dl-btns.png' style='max-width: 200px' />
        <div style='display: flex;justify-content: center; align-items: center'>
            <a href='mailto:komavostore@gmail.com'><img src='https://komavo-dev.s3.ap-south-1.amazonaws.com/gmail.png' style='max-width: 50px;margin: 20px'></a>
            <a href='https://instagram.com/komavo.in?utm_medium=copy_link'><img src='https://komavo-dev.s3.ap-south-1.amazonaws.com/instagram.png' style='max-width: 50px;margin: 20px'></a>
            <a href='https://www.facebook.com/profile.php?id=100070388658410'><img src='https://komavo-dev.s3.ap-south-1.amazonaws.com/facebook.png' style='max-width: 50px;margin: 20px'></a>
        </div>
    </body>
    </html>
    """
    return html_content