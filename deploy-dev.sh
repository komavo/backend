# !/bin/bash
# Get servers list:
set — f
# Variables from GitLab server:
string=$DEV_SERVER_IP

echo "Deploying project on server ${string} komavo-backend OK!"
# ssh ubuntu@${string} "cd projects/dev/kyc-service && git pull origin develop && docker-compose up -d --build && docker system prune -f && cowsay Deploying 3 docker containers is so easy !!!"
ssh ubuntu@${string} "cd komavo/backend && sudo source venv/bin/activate && git pull origin develop && docker rm moja-kyc-api && docker system prune -f && docker run --name=moja-kyc-api -d -p 8083:8083  --network='host' moja-kyc-api && docker ps && cowsay Deploying 3 docker containers are so easy !!!"

echo "All good"
